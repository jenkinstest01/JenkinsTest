import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class FirstTest {
    private static WebDriver driver = new ChromeDriver();
    @BeforeAll
    static void setup() {
        driver = new ChromeDriver(new ChromeOptions());
    }
    @AfterAll
    public void afterTest() {
        driver.close();
        driver.quit();
    }

    @Test
    public void myFirstTest() {
        driver.get("https://google.com/");
        afterTest();
    }
}
